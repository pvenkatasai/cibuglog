var dummy_script = `def replication_check(src_bug, dest_bug):
    """This a very trivial example script"""
    if int(src_bug['bug_id']) % 2 == 0:
        return {"set_fields": {'description': src_bug['description'],'title': src_bug['title']},
                "add_comments": ["hello","world","foo"]}
    else:
        return {}`;

function build_bug_row(bug, bug_data){
  var bug_link = "<td><a href=\"" + bug.url + "\">" + bug.bug_id + "</a></td>"
  var bug_status = "<td>" + bug.status + "</td>"
  var bug_title = "<td>" + bug.title + "</td>"
  var bug_priority = "<td>" + bug.priority + "</td>"
  var bug_operation = "<td>" + bug_data["operation"] + "</td>"
  var bug_fields = "<td>" + JSON.stringify(bug_data["set_fields"]) + "</td>"
  var bug_comments = "<td>" + JSON.stringify(bug_data["add_comments"]) + "</td>"
  return bug_link + bug_status + bug_title + bug_priority + bug_operation + bug_fields + bug_comments
}

function build_update_bug_row(bug, elem){
  var upd_table = $("<table class=\"table\"></table>")
  var tab_head = "<thead><tr><th scope=\"col\">Replicated Bug ID</th>"+
                 "<th scope=\"col\">Status</th><th scope=\"col\">Title</th>"+
                 "<th scope=\"col\">Priority</th></tr></thead>"
  var bug_link = "<td><a href=\"" + bug.url + "\">" + bug.bug_id + "</a></td>"
  var bug_status = "<td>" + bug.status + "</td>"
  var bug_title = "<td>" + bug.title + "</td>"
  var bug_priority = "<td>" + bug.priority + "</td>"
  var full_str = "<tbody><tr>" + bug_link + bug_status + bug_title + bug_priority + "</tr></tbody>"
  upd_table.append(tab_head)
  upd_table.append(full_str)
  return upd_table
}

function python_editor(script_box) {
  var editor = CodeMirror.fromTextArea(script_box, {
    mode: {name: "python",
    version: 3,
    singleLineStringErrors: false},
    lineNumbers: true,
    indentUnit: 4,
    matchBrackets: true,
    extraKeys: {
      "F11": function(cm) {
        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
      },
      "Esc": function(cm) {
        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
      }
    }
  });

  return editor
}

function check_script(scr, src_tracker, dest_tracker, table_id, url, loader, token){
  var params = {
      url: url,
      type: 'POST',
      headers: {
        'X-CSRFToken': token
      },
      data: {
        'script': scr,
        'source_tracker': src_tracker,
        'destination_tracker': dest_tracker
      },
      dataType: 'json',
      beforeSend: function() {
        $(table_id).hide()
        loader.start();
      },
      success: function(data) {
        $(".save_btn").show()
        loader.stop();
        $(table_id).show()
        $.each(data['bugs'], function(i, bug_data) {
          var row = $("<tr></tr>")
          var row_str = build_bug_row(bug_data["src_bug"], bug_data)
          row.html(row_str)

          if (bug_data["operation"] === "update") {
            var upd_col = row.find("td:eq(4)")
            upd_col.text("")
            upd_col.addClass("dropdown")

            var upd_btn = $("<button>update<span class=\"caret\"></span></button>")
            upd_btn.addClass("btn-xs btn-primary dropdown-toggle")
            upd_btn.attr({"type":"button", "data-toggle":"dropdown"})

            var dd_menu = $("<ul></ul>")
            dd_menu.addClass("dropdown-menu center-menu")

            var sub_li = $("<li></li>")
            sub_li.css("position", "relative")

            upd_table = build_update_bug_row(bug_data["dest_bug"], sub_li)
            upd_col.append(upd_btn)
            sub_li.append(upd_table)
            dd_menu.append(sub_li)
            upd_col.append(dd_menu)
          }
          $(table_id).append(row)
        })
      },
      error: function(xhr, ajaxOptions, respError) {
        $(".save_btn").hide()
        $('.modal').modal('hide')
        loader.stop();
        alert("Script execution failure: " + jQuery.parseJSON(xhr.responseText)["message"])
      }
  };

  $.ajax(params)
}

function setup_import_script(import_btn, editor) {
  $(import_btn).on('change', function(){
      var fileReader = new FileReader();
      fileReader.onload = function () {
        var data = fileReader.result
        editor.setValue(data)
      }
      fileReader.readAsText($(import_btn).prop('files')[0])
  })
}

function setup_export_script(export_btn, editor, filename) {
  $(export_btn).click(function(){
    var scr = editor.getValue()
    var fname = filename
    var blob = new Blob([scr], {type:"text/plain;charset=utf-8"})
    saveAs(blob, fname)
  })
}
