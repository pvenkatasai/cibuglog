from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.db import transaction

from rest_framework.decorators import detail_route, api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status, viewsets, permissions

from .serializers import IssueFilterSerializer, RunConfigSerializer, RunConfigDiffSerializer, ComponentSerializer
from .serializers import BuildSerializer, TestSerializer, BugTrackerAccountSerializer, KnownIssuesSerializer
from .serializers import serialize_MetricPassRatePerRunconfig, serialize_MetricPassRatePerTest

from .models import Component, Build, Test, Machine, RunConfigTag, RunConfig, TextStatus
from .models import IssueFilter, Issue, KnownFailure, MachineTag, BugTrackerAccount
from .metrics import MetricPassRatePerRunconfig, MetricPassRatePerTest

import re


class IssueFilterViewSet(viewsets.ModelViewSet):
    queryset = IssueFilter.objects.all().order_by('-id')
    serializer_class = IssueFilterSerializer

    def __check_list__(self, request_data, field, field_name, db_class, errors):
        objects = set(request_data.get(field, []))

        objects_db = dict()
        for obj in db_class.objects.filter(id__in=objects):
            objects_db[obj.id] = obj

        if len(objects) != len(objects_db):
            errors.append("At least one {} does not exist".format(field_name))

        return objects, objects_db

    def __get_or_None__(self, klass, field, request_dict, errors):
        obj = None
        if field in request_dict:
            obj_id = request_dict[field]

            # Do not consider empty strings as meaning a valid value
            if isinstance(obj_id, str) and len(obj_id) == 0:
                return None

            # Convert the id to an int or fail
            try:
                obj_id = int(obj_id)
            except Exception:
                errors.append("The field '{}' needs to be an integer".format(field))
                return None

            # Try getting the object
            obj = klass.objects.filter(id=obj_id).first()
            if obj is None:
                errors.append("The object referenced by '{}' does not exist".format(field))

        return obj

    @transaction.atomic
    def create(self, request):
        errors = []
        if len(request.data.get('description', '')) == 0:
            errors.append("The field 'description' cannot be empty")

        # Check if the filter should replace another one
        edit_filter = self.__get_or_None__(IssueFilter, 'edit_filter',
                                           request.data, errors)
        edit_issue = self.__get_or_None__(Issue, 'edit_issue',
                                          request.data, errors)

        # Check that all the tags, machines, tests, and statuses are present
        tags, tags_db = self.__check_list__(request.data, "tags", "tag", RunConfigTag, errors)
        machine_tags, machine_tags_db = self.__check_list__(request.data, "machine_tags", "machine tag",
                                                            MachineTag, errors)
        machines, machines_db = self.__check_list__(request.data, "machines", "machine", Machine, errors)
        tests, tests_db = self.__check_list__(request.data, "tests", "test", Test, errors)
        statuses, statuses_db = self.__check_list__(request.data, "statuses", "status", TextStatus, errors)

        # Check the regular expressions
        for field in ['stdout_regex', 'stderr_regex', 'dmesg_regex']:
            try:
                re.compile(request.data.get(field, ""))
            except Exception:
                errors.append("The field '{}' does not contain a valid regular expression".format(field))

        # Create the object or fail depending on whether we got errors or not
        if len(errors) == 0:
            filter = IssueFilter.objects.create(description=request.data.get('description'),
                                                stdout_regex=request.data.get('stdout_regex', ""),
                                                stderr_regex=request.data.get('stderr_regex', ""),
                                                dmesg_regex=request.data.get('dmesg_regex', ""))

            filter.tags.add(*tags_db)
            filter.machines.add(*machines_db)
            filter.machine_tags.add(*machine_tags_db)
            filter.tests.add(*tests_db)
            filter.statuses.add(*statuses_db)

            # If this filter is supposed to replace another filter
            if edit_filter is not None:
                if edit_issue is not None:
                    edit_issue.replace_filter(edit_filter, filter, request.user)
                else:
                    edit_filter.replace(filter, request.user)

            serializer = IssueFilterSerializer(filter)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class RunConfigViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RunConfig.objects.all().order_by('-id')
    serializer_class = RunConfigSerializer

    @classmethod
    def known_failures_serialized(cls, runcfg):
        f = KnownFailure.objects.filter(result__ts_run__runconfig=runcfg)
        failures = f.prefetch_related('result__status', 'result__test', 'result__test__testsuite',
                                      'result__ts_run__machine', 'matched_ifa__issue__bugs',
                                      'matched_ifa__issue__bugs__tracker')
        return KnownIssuesSerializer(failures, read_only=True, many=True)

    def _get_runcfg(self, obj_id):
        try:
            return RunConfig.objects.get(pk=int(obj_id))
        except ValueError:
            pass

        return get_object_or_404(RunConfig, name=obj_id)

    @detail_route()
    def known_failures(self, request, pk=None):
        runcfg = self._get_runcfg(pk)
        return Response(self.known_failures_serialized(runcfg).data, status=status.HTTP_200_OK)

    @detail_route()
    def compare(self, request, pk=None):
        runcfg_from = self._get_runcfg(pk)
        runcfg_to = self._get_runcfg(request.GET.get('to'))
        no_compress = request.GET.get('no_compress') is not None

        diff = runcfg_from.compare(runcfg_to, no_compress=no_compress)
        if request.GET.get('summary') is None:
            serializer = RunConfigDiffSerializer(diff)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return HttpResponse(diff.text)


class ComponentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Component.objects.all().order_by('-id')
    serializer_class = ComponentSerializer


class BuildViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Build.objects.all().order_by('-id')
    serializer_class = BuildSerializer


class TestSet(viewsets.ReadOnlyModelViewSet):
    queryset = Test.objects.all().order_by('-id')
    serializer_class = TestSerializer
    filter_fields = ('id', 'testsuite', 'public', 'vetted_on', 'name')


class BugTrackerAccountViewSet(viewsets.ModelViewSet):
    # WARNING: we do not yet perform access control for setting who is a user or developer because of the limited damage
    # this can cause and the annoyance of having to ask large group of users to authenticate then be granted the
    # privilege to change the roles
    permission_classes = []
    authentication_classes = []

    queryset = BugTrackerAccount.objects.all().order_by('-id')
    serializer_class = BugTrackerAccountSerializer

    http_method_names = ['get', 'patch']


@api_view()
@permission_classes((permissions.AllowAny,))
def metrics_passrate_per_runconfig_view(request):
    history = MetricPassRatePerRunconfig(request.GET.copy())
    return Response(serialize_MetricPassRatePerRunconfig(history))


@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def metrics_passrate_per_test_view(request):
    params = request.POST if request.method == 'POST' else request.GET
    passrate = MetricPassRatePerTest(params.copy())
    return Response(serialize_MetricPassRatePerTest(passrate))
