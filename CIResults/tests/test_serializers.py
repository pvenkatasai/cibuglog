from django.test import TestCase

from CIResults.metrics import Rate
from CIResults.serializers import RateSerializer


class RateSerializerTests(TestCase):
    def test_basic(self):
        rate = Rate(count=5, total=10)
        self.assertEqual(RateSerializer(rate).data,
                         {'count': 5, 'total': 10, 'percent': 50.0})

    def test_empty(self):
        rate = Rate(count=0, total=0)
        self.assertEqual(RateSerializer(rate).data,
                         {'count': 0, 'total': 0, 'percent': 0.0})
