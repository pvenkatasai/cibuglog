from django.test import TestCase

from CIResults.models import BugTracker
from CIResults.metrics import Periodizer, Period, bugs_followed_since, Rate

import datetime
import pytz


class PeriodizerTests(TestCase):
    def __common_checks(self, periods):
        self.assertEqual(periods[0].start, datetime.datetime(2018, 3, 19, 0, 0))
        self.assertEqual(periods[0].end, datetime.datetime(2018, 3, 26, 0, 0))
        self.assertEqual(periods[-1].start, datetime.datetime(2018, 10, 8, 0, 0))
        self.assertEqual(periods[-1].end, datetime.datetime(2018, 10, 15, 0, 0))

        self.assertEqual(len(periods), 30)

        # Check that the time between each day is the expected one
        for p, period in enumerate(periods):
            self.assertEqual(period.end - period.start, datetime.timedelta(days=7))
            if p > 1:
                self.assertEqual(period.start, periods[p-1].end)

    def test_monday_midnight(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=8,
                                                             hour=0, minute=0, second=0)))
        self.__common_checks(periods)

    def test_wednesday(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=10,
                                                             hour=14, minute=23, second=10)))
        self.__common_checks(periods)

    def test_sunday_1s_to_midnight(self):
        periods = list(Periodizer(end_date=datetime.datetime(year=2018, month=10, day=14,
                                                             hour=23, minute=59, second=59)))
        self.__common_checks(periods)

    def __from_json(self, json_str, expected, description=None):
        period = Periodizer.from_json(json_str)

        self.assertEqual(list(period), expected)
        if description is not None:
            self.assertEqual(period.description, description)

        return period

    def test_from_json__days(self):
        expected = [
            Period(datetime.datetime(2019, 7, 8, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 9, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 7, 9, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 10, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 7, 10, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 11, 0, 0, tzinfo=pytz.utc))
        ]
        self.__from_json('{"period": "day", "count": 3, "end": "2019-07-10 06:06:06.123456"}', expected,
                         'last 3 days before 2019-07-11')

    def test_from_json__days_from_now(self):
        period = Periodizer.from_json('{"period": "day", "count": 3}')
        self.assertEqual(len(list(period)), 3)
        self.assertNotIn('before', period.description)

    def test_from_json__weeks(self):
        expected = [
            Period(datetime.datetime(2019, 6, 24, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 1, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 7, 1, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 8, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 7, 8, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 15, 0, 0, tzinfo=pytz.utc)),
        ]
        description = 'last 3 weeks before 2019-07-15 / WW-2019.28'
        self.__from_json('{"period": "week", "count": 3, "end": "2019-07-10 06:06:06.123456"}', expected,
                         description)
        self.__from_json('{"period": "invalid", "count": 3, "end": "2019-07-10 06:06:06.123456"}', expected,
                         description)
        self.__from_json('{"count": 3, "end": "2019-07-10 06:06:06.123456"}', expected, description)

    def test_from_json__months(self):
        expected = [
            Period(datetime.datetime(2019, 5, 1, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 6, 1, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 6, 1, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 7, 1, 0, 0, tzinfo=pytz.utc)),
            Period(datetime.datetime(2019, 7, 1, 0, 0, tzinfo=pytz.utc),
                   datetime.datetime(2019, 8, 1, 0, 0, tzinfo=pytz.utc)),
        ]
        self.__from_json('{"period": "month", "count": 3, "end": "2019-07-10 06:06:06.123456"}', expected,
                         'last 3 months before August 2019')


class bugs_followed_sinceTests(TestCase):
    def test_no_bugtrackers(self):
        self.assertEqual(bugs_followed_since(), None)

    def test_one_bugtrackers_without_components_followed_since(self):
        BugTracker.objects.create(public=True)
        self.assertEqual(bugs_followed_since(), None)

    def test_one_bugtrackers_with_components_followed_since(self):
        followed_since = datetime.datetime(2019, 5, 1, 0, 0, tzinfo=pytz.utc)
        BugTracker.objects.create(public=True, components_followed_since=followed_since)
        self.assertEqual(bugs_followed_since(), followed_since)

    def test_two_bugtrackers_with_components_followed_since(self):
        followed_since1 = datetime.datetime(2019, 5, 1, 0, 0, tzinfo=pytz.utc)
        followed_since2 = datetime.datetime(2019, 5, 2, 0, 0, tzinfo=pytz.utc)

        BugTracker.objects.create(public=True, name='bt1', components_followed_since=followed_since2)
        BugTracker.objects.create(public=True, name='bt2', components_followed_since=followed_since1)

        self.assertEqual(bugs_followed_since(), followed_since2)


class RateTests(TestCase):
    def test_basic(self):
        rate = Rate(count=5, total=10)
        self.assertEqual(rate.count, 5)
        self.assertEqual(rate.total, 10)
        self.assertEqual(rate.percent, 50.0)

    def test_empty(self):
        rate = Rate(count=0, total=0)
        self.assertEqual(rate.count, 0)
        self.assertEqual(rate.total, 0)
        self.assertEqual(rate.percent, 0.0)
